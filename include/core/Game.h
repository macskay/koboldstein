#pragma once

class BaseScene;
class AI;

#include <SFML/Graphics.hpp>
#include "Environment.h"

#include "scenes/BaseScene.h"
#include "network/Server.h"
#include "ai/AI.h"

class Game
{
public:
	Game();
	virtual ~Game();

	void createWindow(uint32_t width, uint32_t height);
	void enterMainloop();
	void runMainloop();

	void startGame();
    void startServer(const bool& ai);
	void exitGame();
	void pushScene(BaseScene* scene);

	sf::RenderWindow& getWindow();
private:
	std::unique_ptr<sf::RenderWindow> mainWindow;
	std::stack<BaseScene*> scenes;
    std::unique_ptr<Server> server;
    std::thread t1;
    std::unique_ptr<AI> aiClient;
};


