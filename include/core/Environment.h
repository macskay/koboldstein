#pragma once
 
///// INCLUDE /////

#include <iostream>
#include <stack>
#include <thread>
#include <string>
#include <functional>
#include <memory>
#include <cerrno>
#include <zmq.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

#ifndef _WIN32
	#include <unistd.h>
#else
	// #include <windows.h>
	#define sleep(n) Sleep(n)
#endif

///// NETWORKING /////

#define MSG_SIZE 256

///// GAME /////

#define TROUGH_SIZE 135
#define STARTING_STONES 2
#define COLS 8
#define ROWS 4

///// RENDERING /////

#define MARGIN_X 100
#define MARGIN_Y 90


