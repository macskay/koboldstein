#pragma once

#include "gui/Button.h"

class ExitButton :
	public Button
{
public:
	ExitButton(Game& game_) : Button(game_) {}
	~ExitButton() {}

	void buttonHit();
};

