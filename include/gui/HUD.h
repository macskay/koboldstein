#pragma once

#include "core/Game.h"

class HUD
{
public:
	HUD(Game& game_);
	~HUD();

	void setupFont();
	void setupTurnLabel();

	void setTurn(bool playerTurn);

	void update(const sf::Int64 deltaTime);
	void render(const sf::Int64 deltaTime);
private:
	Game& game;

	sf::Font font;
	sf::Text turn;
};

