#pragma once

#include "core/Game.h"
#include "scenes/ClientScene.h"
#include "core/Environment.h"

class Button : public sf::Sprite
{
public:
	Button(Game& game);
	~Button();
	Button(const Button& other);

	void loadFont();
	void setupText();

	void startMouseOver();
	void endMouseOver();
	void mouseDown();
	void mouseUp();

	Button& operator=(const Button& other);
	void setText(const sf::String txt);

	void update(const sf::Int64 deltaTime);
	void render(const sf::Int64 deltaTime);
	void setTextures(const std::map<std::string, sf::Texture>& textures);

	virtual void buttonHit() = 0;

	std::reference_wrapper<Game> getGame();
private:
	std::map<std::string, sf::Texture> textures;
	std::reference_wrapper<Game> game;

	sf::Text text;
	sf::Font font;

	bool pressed;
	bool mouseOver;
};

