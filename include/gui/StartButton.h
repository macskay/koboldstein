#pragma once

#include "gui/Button.h"

class StartButton :
	public Button
{
public:
	StartButton(Game& game_) : Button(game_) {}
	~StartButton() {}

	void buttonHit();
};

