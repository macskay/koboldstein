#pragma once

class Game;

#include "core/Game.h"

class BaseScene
{
public:
	explicit BaseScene(Game& game_);
	virtual ~BaseScene();

	virtual void update(const sf::Int64& deltaTime) {};
	virtual void render(const sf::Int64& deltaTime) {};
	virtual void handleInput(const sf::Event& evt) {};
	void handleEvents();

	static sf::Texture loadTexture(std::string path, sf::IntRect size);
	virtual void loadTextures() {};

	sf::RenderWindow& getWindow();

protected:
	Game& game;
};

