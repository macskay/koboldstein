#pragma once

#include "scenes/BaseScene.h"
#include "gui/ExitButton.h"
#include "gui/StartButton.h"

class MenuScene :
	public BaseScene
{
public:
	MenuScene(Game& game);
	virtual ~MenuScene();

	void setupButtons();
	void setupStartButton();
	void setupExitButton();

	void update(const sf::Int64& deltaTime) override;
	void render(const sf::Int64& deltaTime) override;
	void renderBackdrop(const sf::Int64& deltaTime);

	void handleInput(const sf::Event& evt);

	void loadTextures();
private:
	std::map<std::string, sf::Texture> textures;
	std::vector<std::shared_ptr<Button>> buttons;
};

