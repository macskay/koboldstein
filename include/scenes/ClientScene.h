#pragma once

#include <SFML/Graphics.hpp>
#include "core/Environment.h"
#include "scenes/BaseScene.h"
#include "network/Client.h"
#include "gui/HUD.h"


class ClientScene : 
    public BaseScene
{
public:
    explicit ClientScene(Game& game);

    void update(const sf::Int64& deltaTime) override;
    void render(const sf::Int64& deltaTime) override;
    void handleInput(const sf::Event& evt) override;

    void loadTextures() override;

    void renderBoard(const sf::Int64& deltaTime);
private:
    std::unique_ptr<Client> client;
    std::map<std::string, sf::Texture> textures;
};
