#pragma once

#include <zmq.hpp>
#include <msgpack11.hpp>

using namespace msgpack11;

class Messenger {
public:
    virtual MsgPack receiveMsgPack(zmq::socket_t& socket);
    virtual void publishMsgPack(zmq::socket_t& socket, const MsgPack& msgPack);
};

