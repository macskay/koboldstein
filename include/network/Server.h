#pragma once

#include "core/Environment.h"
#include "network/Messenger.h"
#include <array>

using namespace msgpack11;

class Server : public Messenger
{
public:
    Server();
    void start();

    void registerPlayer(const MsgPack& msgPack);
    void startingGame();
    void executeTurn(MsgPack& msgPack);
    void updateGameState(MsgPack& msgPack);
    static MsgPack getErrorMsgPack(const std::string& msg, const std::string& recipient);
    bool isItPlayersTurn(const std::string& playerId);
    void endTurn();

    std::string serializeBoard();

private:
    bool gameStarted;
    bool serverRunning;

    zmq::context_t context;
    zmq::socket_t pull;
    zmq::socket_t pub;

    std::string currentPlayer;
	std::vector<std::string> playerIds;
    std::array<std::array<int, 16>, 2> board;
};
