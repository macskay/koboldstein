#pragma once

#include "core/Environment.h"
#include "network/Messenger.h"

using namespace msgpack11;

class Client : public Messenger
{
public:
    Client();
    explicit Client(std::string);

    void registerWithServer(std::string playerType);
    MsgPack createMsgPack(std::string type, std::string msg);
    void pushMessageToServer(const MsgPack& msgPack);
    void update();
    bool isRecipient(const std::string& recp);

    std::string getId();
    void deserializeBoard(std::string serializedBoard);
private:
    zmq::context_t context;
    zmq::socket_t push;
    zmq::socket_t sub;

    std::array<std::array<int, 16>, 2> board;
    boost::uuids::uuid id;
};
