#pragma once

class Game;

#include "core/Game.h"
#include "core/Environment.h"
#include "network/Client.h"
#include <SFML/Graphics.hpp>

class AI
{
public:
    explicit AI();
    void update(const sf::Int64& deltaTime);

private:
    std::unique_ptr<Client> client;
};