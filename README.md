Dependencies
============

Koboldstein benötigt zum Bauen die folgenden Dependencies:
 - SFML (https://www.sfml-dev.org/index.php)
 - ZeroMQ (http://zeromq.org/)
 - MessagePack (https://msgpack.org/)

## Linux
--------


SFML
####

~~~bash
sudo apt install libsfml-dev
~~~

ZeroMQ
######

~~~bash
echo "deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/ ./" >> /etc/apt/sources.list
wget https://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/Release.key -O- | sudo apt-key add
apt-get install libzmq3-dev
~~~

MessagePack
###########

~~~bash
git clone git@github.com:ar90n/msgpack11.git
mkdir build
cd build
cmake ../msgpack11
make && make install
~~~


Installation
============

~~~bash
$ make -j9
~~~

