# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/max/Workspace/KoboldsteinCpp/src/Main.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/Main.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/ai/AI.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/ai/AI.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/core/Game.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/core/Game.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/gui/Button.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/gui/Button.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/gui/ExitButton.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/gui/ExitButton.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/gui/HUD.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/gui/HUD.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/gui/StartButton.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/gui/StartButton.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/network/Client.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/network/Client.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/network/Messenger.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/network/Messenger.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/network/Server.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/network/Server.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/scenes/BaseScene.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/scenes/BaseScene.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/scenes/ClientScene.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/scenes/ClientScene.cpp.o"
  "/home/max/Workspace/KoboldsteinCpp/src/scenes/MenuScene.cpp" "/home/max/Workspace/KoboldsteinCpp/cmake-build-debug/CMakeFiles/Main.dir/src/scenes/MenuScene.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
