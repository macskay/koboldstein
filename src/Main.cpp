#include "core/Game.h"

int main()
{
	Game game;
	game.createWindow(1280, 720);
	game.startGame();
	game.enterMainloop();

	return 0;
}
