#include "scenes/ClientScene.h"

ClientScene::ClientScene(Game& game) :
    BaseScene(game),
    client(std::make_unique<Client>())
{
    client->registerWithServer("player");

    loadTextures();
}

void ClientScene::loadTextures()
{
    textures.insert(std::make_pair("Board", loadTexture("gfx/board.png", sf::IntRect(0, 0, game.getWindow().getSize().x, game.getWindow().getSize().y))));
    textures.insert(std::make_pair("Trough", loadTexture("gfx/trough.png", sf::IntRect(0, 0, TROUGH_SIZE, TROUGH_SIZE))));
}

void ClientScene::update(const sf::Int64& deltaTime)
{
    client->update();
}

void ClientScene::renderBoard(const sf::Int64& deltaTime)
{
    sf::Sprite sprite;
    sprite.setTexture(textures["Board"]);

    game.getWindow().draw(sprite);
}

void ClientScene::render(const sf::Int64& deltaTime)
{
    renderBoard(deltaTime);
}

void ClientScene::handleInput(const sf::Event& evt)
{
    if (evt.type == sf::Event::KeyPressed)
        if (evt.key.code == sf::Keyboard::S)
            client->pushMessageToServer(client->createMsgPack("start", "start"));
    if (evt.type == sf::Event::MouseButtonPressed)
        if (evt.mouseButton.button == sf::Mouse::Left)
            client->pushMessageToServer(client->createMsgPack("turn", "5"));
}