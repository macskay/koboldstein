#include "scenes/MenuScene.h"



MenuScene::MenuScene(Game& game_) : BaseScene(game_)
{
	loadTextures();
	setupButtons();
}

MenuScene::~MenuScene()
{
}


void MenuScene::setupButtons()
{
	setupStartButton();
	setupExitButton();
}

void MenuScene::setupStartButton()
{
	std::shared_ptr<Button> startButton = std::make_shared<StartButton>(game);
	startButton->setScale(1.5, 1);
	startButton->setTextures(textures);
	startButton->setTexture(textures["ButtonUp"]);
	startButton->setPosition(textures["Backdrop"].getSize().x / 2 - startButton->getGlobalBounds().width / 2,
		textures["Backdrop"].getSize().y / 2 - startButton->getGlobalBounds().height / 2);
	startButton->setText("Start Game vs. AI");

	buttons.push_back(startButton);
}

void MenuScene::setupExitButton()
{
	std::shared_ptr<Button> exitButton = std::make_shared<ExitButton>(game);
	exitButton->setScale(1.5, 1);
	exitButton->setTextures(textures);
	exitButton->setTexture(textures["ButtonUp"]);
	exitButton->setPosition(textures["Backdrop"].getSize().x / 2 - exitButton->getGlobalBounds().width / 2,
		textures["Backdrop"].getSize().y / 2 - exitButton->getGlobalBounds().height / 2 + exitButton->getGlobalBounds().height);
	exitButton->setText("Exit Button");

	buttons.push_back(exitButton);
}

void MenuScene::update(const sf::Int64& deltaTime)
{
	for (std::shared_ptr<Button> b : buttons)
		b->update(deltaTime);
}


void MenuScene::render(const sf::Int64& deltaTime)
{
	renderBackdrop(deltaTime);
	for (std::shared_ptr<Button> b : buttons)
		b->render(deltaTime);
}


void MenuScene::handleInput(const sf::Event& evt)
{
	sf::Vector2f mousePos = game.getWindow().mapPixelToCoords(sf::Mouse::getPosition(game.getWindow()));
	for (std::shared_ptr<Button>& b : buttons) {
        if (evt.type == sf::Event::MouseMoved)
            (b->getGlobalBounds().contains(mousePos) ? b->startMouseOver() :  b->endMouseOver());

        if (evt.type == sf::Event::MouseButtonPressed)
            if (evt.mouseButton.button == sf::Mouse::Left)
                if (b->getGlobalBounds().contains(mousePos))
                    b->mouseDown();

        if (evt.type == sf::Event::MouseButtonReleased)
            if (evt.mouseButton.button == sf::Mouse::Left)
                (b->getGlobalBounds().contains(mousePos) ? b->buttonHit() : b->mouseUp());
    }
}


void MenuScene::loadTextures()
{
	textures.insert(std::make_pair("Backdrop", loadTexture("gfx/backdrop.jpg", sf::IntRect(0, 0, 1280, 720))));
	textures.insert(std::make_pair("BackdropShade", loadTexture("gfx/shade.png", sf::IntRect(0, 0, 64, 48))));
	textures.insert(std::make_pair("ButtonUp", loadTexture("gfx/btn_up.png", sf::IntRect(0, 0, 128, 32))));
	textures.insert(std::make_pair("ButtonDown", loadTexture("gfx/btn_down.png", sf::IntRect(0, 0, 128, 32))));
	textures.insert(std::make_pair("ButtonOver", loadTexture("gfx/btn_over.png", sf::IntRect(0, 0, 128, 32))));
}

void MenuScene::renderBackdrop(const sf::Int64& deltaTime)
{
	sf::Sprite sprite;
	sprite.setTexture(textures["Backdrop"]);

	game.getWindow().draw(sprite);
}

