#include "scenes/BaseScene.h"


BaseScene::BaseScene(Game& game_) 
	: game(game_)
{
}

BaseScene::~BaseScene()
{
}

void BaseScene::handleEvents()
{
	sf::Event evt;
	while (getWindow().pollEvent(evt))
	{
		evt.type == sf::Event::Closed ? getWindow().close() : handleInput(evt);
	}
}

sf::Texture BaseScene::loadTexture(std::string path, sf::IntRect size)
{
	sf::Texture texture;
	texture.loadFromFile("../res/" + path, size);

	return texture;
}

sf::RenderWindow& BaseScene::getWindow()
{
	return game.getWindow();
}
