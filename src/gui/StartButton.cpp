#include "gui/StartButton.h"

void StartButton::buttonHit()
{
    getGame().get().startServer(true);
    getGame().get().pushScene(new ClientScene(getGame()));
}
