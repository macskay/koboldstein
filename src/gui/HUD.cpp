#include "gui/HUD.h"


HUD::HUD(Game& game_)
	: game(game_)
{
	setupFont();
	setupTurnLabel();
}


HUD::~HUD()
{
}

void HUD::update(const sf::Int64 deltaTime)
{
}

void HUD::render(const sf::Int64 deltaTime)
{
	game.getWindow().draw(turn);
}

void HUD::setupTurnLabel()
{
	turn.setFillColor(sf::Color::White);
	turn.setCharacterSize(50);
	turn.setFont(font);
	turn.setPosition(0, 0);
}

void HUD::setupFont()
{
	font.loadFromFile("cuckoo.ttf");
}

void HUD::setTurn(bool playerTurn)
{
	turn.setString(playerTurn ? "Turn: Player" : "Turn: AI");
}

