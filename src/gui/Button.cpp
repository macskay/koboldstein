#include "gui/Button.h"


Button::Button(Game& game_)
	: game(game_), pressed(false), mouseOver(false)
{
	loadFont();
	setupText();
}

Button::~Button()
{
}

void Button::render(const sf::Int64 deltaTime)
{
	game.get().getWindow().draw(*this);
	game.get().getWindow().draw(text);
}

void Button::update(const sf::Int64 deltaTime)
{
	setTexture(pressed ? textures["ButtonDown"] :
		(mouseOver ? textures["ButtonOver"] : textures["ButtonUp"]));
}

Button::Button(const Button& other)
	: game(other.game)
{
}

Button& Button::operator=(const Button& other)
{
	if (this != &other)
		game = other.game;
	return *this;
}

void Button::setText(const sf::String txt)
{
	text.setString(txt);

	sf::Vector2f dimensions = sf::Vector2f(text.getGlobalBounds().width, text.getGlobalBounds().height);
	
	text.setPosition(getPosition().x + getGlobalBounds().width / 2 - dimensions.x / 2, getPosition().y + getGlobalBounds().height / 2 - dimensions.y);

}

void Button::loadFont()
{
	font.loadFromFile("../res/fonts/cuckoo.ttf");
}

void Button::setupText()
{
	text.setFillColor(sf::Color::Black);
	text.setCharacterSize(20);
	text.setFont(font);
}

void Button::startMouseOver()
{
	mouseOver = true;
}

void Button::endMouseOver()
{
	setTexture(textures["ButtonUp"]);
	mouseOver = false;
}

void Button::mouseDown()
{
	pressed = true;
}

void Button::mouseUp()
{
	pressed = false;
}

void Button::setTextures(const std::map<std::string, sf::Texture>& textures_)
{
	textures = textures_;
}

void Button::buttonHit()
{
	std::cout << "Pressed Button" << std::endl;
}

std::reference_wrapper<Game> Button::getGame()
{
	return game;
}
