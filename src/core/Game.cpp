#include "core/Game.h"
#include "scenes/MenuScene.h"

Game ::Game()
{
}

Game::~Game()
{
}

void Game::createWindow(uint32_t width, uint32_t height)
{
	mainWindow = std::make_unique<sf::RenderWindow>(sf::VideoMode(width, height), "Koboldstein");
}

void Game::enterMainloop()
{
	try 
	{
		runMainloop();
	}
	catch (std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
	}
}

void Game::runMainloop()
{
	sf::Clock clock;
	sf::Time accumulator = sf::Time::Zero;
	sf::Time ups = sf::seconds(1.f / 60.f);
	while (mainWindow->isOpen())
	{
		sf::Time time = clock.getElapsedTime();

		BaseScene* scene = scenes.top();
		scene->handleEvents();

		while (accumulator > ups)
		{
			accumulator -= ups;
			scene->update(time.asMicroseconds());
            if(aiClient)
                aiClient->update(time.asMicroseconds());
		}

		mainWindow->clear();
		scene->render(1.0 - accumulator / ups);
		mainWindow->display();
		
		accumulator += clock.restart();
	}
}

void Game::pushScene(BaseScene* scene)
{
	scenes.push(scene);
}

void Game::startGame()
{
    pushScene(new MenuScene(*this));

}

void Game::exitGame()
{
	mainWindow->close();
}

sf::RenderWindow& Game::getWindow()
{
	return *mainWindow;
}

void Game::startServer(const bool& ai)
{
    server = std::make_unique<Server>();
    t1 = std::thread(&Server::start, server.get());
    if (ai)
        aiClient = std::make_unique<AI>();
}


