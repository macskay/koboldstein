#include "network/Server.h"

Server::Server()
    : context(1), pub(context, ZMQ_PUB), pull(context, ZMQ_PULL), gameStarted(false), serverRunning(true)
{
    pub.bind("tcp://*:5557");
    pull.bind("tcp://*:5558");
}

void Server::start()
{
    while (serverRunning)
    {
        MsgPack msgPack = receiveMsgPack(pull);
        if (msgPack["type"].string_value() == "register")
            registerPlayer(msgPack);
        else if (msgPack["type"].string_value() == "start")
            startingGame();
        else if (msgPack["type"].string_value() == "turn")
            executeTurn(msgPack);
    }
}

void Server::registerPlayer(const MsgPack& msgPack)
{
    std::cout << "Registering Player: " << msgPack["id"].string_value() << " (" + msgPack["client"].string_value() + ")" << std::endl;
    playerIds.push_back(msgPack["id"].string_value());
}

void Server::startingGame()
{
    if (!gameStarted)
    {
        currentPlayer = playerIds[1];
        std::cout << "Starting game with " << playerIds.size() << " players" << std::endl;
        std::cout << "Starting player is " << currentPlayer << std::endl;

        for (auto& row : board)
            row = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};

        MsgPack msgPack = MsgPack::object {
                { "type", "update" },
                { "msg", serializeBoard() },
        };
        publishMsgPack(pub, msgPack);
        gameStarted = true;
    }
}

std::string Server::serializeBoard()
{
    std::string boardSerialize;
    for (auto &row : board)
        for (auto& col : row)
            boardSerialize += std::to_string(col);
    return boardSerialize;
}

void Server::endTurn()
{
    currentPlayer = (currentPlayer == playerIds[1]) ? playerIds[0] : playerIds[1];
}

void Server::executeTurn(MsgPack& msgPack)
{
    if (gameStarted)
        updateGameState(msgPack);
    else
        std::cerr << "Game hasn't started yet!" << std::endl;
}

void Server::updateGameState(MsgPack& msgPack)
{
    if (!isItPlayersTurn(msgPack["id"].string_value()))
        msgPack = getErrorMsgPack("It's not your turn!", msgPack["id"].string_value());
    else
    {
        auto it = std::find(playerIds.begin(), playerIds.end(), msgPack["id"].string_value());
        int index = it - playerIds.begin();
        int selected = boost::lexical_cast<int>(msgPack["msg"].string_value()) + index*7;

        std::cout << "Player Index: " << std::to_string(index) << std::endl;
        std::cout << "Selected Trough: "  << std::to_string(selected) << std::endl;
        std::cout << "Moving " << std::to_string(board[index][selected]) << " Troughs" << std::endl;

        /*
         TODO: Trough Logic
         */

        endTurn();
    }
    publishMsgPack(pub, msgPack);
}

bool Server::isItPlayersTurn(const std::string& playerId)
{
    return playerId == currentPlayer;
}

MsgPack Server::getErrorMsgPack(const std::string& msg, const std::string& recipient)
{
    return MsgPack::object {
            { "type", "error" },
            { "recp", recipient.substr(recipient.length() - 12) },
            { "msg", msg },
    };
}

