#include "network/Client.h"
#include <string>

Client::Client() : Client("127.0.0.1")
{
}

Client::Client(std::string host) : context(1), sub(context, ZMQ_SUB), push(context, ZMQ_PUSH), id(boost::uuids::random_generator()())
{
    sub.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    sub.connect("tcp://" + host + ":5557");
    push.connect("tcp://" + host + ":5558");
}

void Client::registerWithServer(std::string playerType)
{
    MsgPack msgPack = msgpack11::MsgPack::object{
            { "id", getId() },
            { "type", "register" },
            { "client", playerType },
    };
    publishMsgPack(push, msgPack);
}

std::string Client::getId()
{
    return boost::lexical_cast<std::string>(id);
}

MsgPack Client::createMsgPack(std::string type, std::string msg)
{
    return MsgPack::object{
            { "id", getId() },
            { "type", type },
            { "msg", msg },
    };
}

bool Client::isRecipient(const std::string& recp)
{
    return recp == getId().substr(getId().length() - 12);
}

void Client::update()
{
    MsgPack msgPack = receiveMsgPack(sub);
    if (msgPack["type"].string_value() == "error" && isRecipient(msgPack["recp"].string_value()))
        std::cerr << "ERROR: " << msgPack["msg"].string_value() << std::endl;
    else if (msgPack["type"].string_value() == "update")
        deserializeBoard(msgPack["msg"].string_value());
}

void Client::pushMessageToServer(const MsgPack &msgPack)
{
    publishMsgPack(push, msgPack);
}

void Client::deserializeBoard(std::string serializedBoard)
{
    for (int i = 0; i < board.size(); ++i)
        for (int j = 0; j < board[0].size(); ++j)
            board[i][j] = boost::lexical_cast<int>(serializedBoard[i * board.size() + j]);
}
