#include "network/Messenger.h"


void Messenger::publishMsgPack(zmq::socket_t& socket, const MsgPack& msgPack)
{
    zmq::message_t reply(256);
    std::string rep = msgPack.dump();
    memcpy(reply.data(), rep.c_str(), 256);
    socket.send(reply);
}


MsgPack Messenger::receiveMsgPack(zmq::socket_t& socket)
{
    zmq::message_t recv;
    socket.recv(&recv, ZMQ_NOBLOCK);

    std::string msg = std::string((char*) recv.data());
    std::string err;
    return MsgPack::parse(msg, err);
}