src = $(wildcard src/*.cpp) \
	  $(wildcard src/core/*.cpp) \
	  $(wildcard src/gui/*.cpp) \
	  $(wildcard src/network/*.cpp) \
	  $(wildcard src/scenes/*.cpp)
obj = $(patsubst src/%.cpp,build/%.o,$(src))

LDSFML := -lsfml-graphics -lsfml-window -lsfml-window -lsfml-system
LDZMQ := -lzmq
LDMSGPACK := -lmsgpack11
LDFLAGS := $(LDSFML) $(LDZMQ) $(LDMSGPACK)
OPTS := -std=c++17
CXXFLAGS := -MMD -include $(obj:.o=.d)

Koboldstein: $(obj)
	$(CXX) $(OPTS) -o bin/$@ -Wall $^ -Iinclude/ $(LDFLAGS) -pthread

build/%.o: src/%.cpp
	$(CXX) $(OPTS) -c $< -o $@ -Iinclude/ $(LDFLAGS)

.PHONY: clean
clean:
	rm -f $(obj) bin/Koboldstein
